__          __  _     _                _____        __ _                          _____       _ _
\ \        / / | |   | |              / ____|      / _| |                        |  __ \     | (_)
 \ \  /\  / /__| |__ | |_ _ __ __ _  | (___   ___ | |_| |___      ____ _ _ __ ___| |  | | ___| |___   _____ _ __ _   _
  \ \/  \/ / _ \ '_ \| __| '__/ _` |  \___ \ / _ \|  _| __\ \ /\ / / _` | '__/ _ \ |  | |/ _ \ | \ \ / / _ \ '__| | | |
   \  /\  /  __/ |_) | |_| | | (_| |_ ____) | (_) | | | |_ \ V  V / (_| | | |  __/ |__| |  __/ | |\ V /  __/ |  | |_| |
    \/  \/ \___|_.__/ \__|_|  \__,_(_)_____/ \___/|_|  \__| \_/\_/ \__,_|_|  \___|_____/ \___|_|_| \_/ \___|_|   \__, |
                                                                                                                  __/ |
                                                                                                                 |___/
-----------------------------------------------------------------------------------------------------------------------

  What is it?
  -----------

  Webtra.SoftwareDelivery is a TripleTower wapp which offers software
  components for download via HTTPS. Webtra.SoftwareDelivery is open source
  software and distributed under MIT license. The software was initially
  written by Glen Cheney in 2014 and heavily modfied by the Webtranet Affinity
  Group in 2018.


  The Latest Version
  ------------------

  Details of the latest version can be found on the Webtranet Affinity Groups's
  Gitlab repository: https://gitlab.com/webtranet/Webtra.SoftwareDelivery


  Documentation
  -------------

  The documentation available as of the date of this release can be found on
  the Webtranet Affinity Group's website in the documentation section:
  https://webtranet.online/wappstower/Webtra.Docs


  Installation
  ------------

  We recommend using the hanoi software installer which is currently available
  for linux and windows.

  Licensing
  ---------

  Webtra.SoftwareDelivery is open source software and distributed under MIT
  license. Please see the file called LICENSE.

  The icons used are licensed under ISC by John Gardner. See according
  LICENSE file.
